---
layout: markdown_page
title: "Content Team"
---

The Content Team is part of the Marketing Team.

## Expertise

* Content Strategy
* [Blog](/blog/) content, design and UX
* Social promotion of blog content
* Newsletters
* [Documentation](https://docs.gitlab.com) and training
* White paper sourcing
* Ebooks
* Enterprise surveys
* Social Media Management
* Sub-editing
* Webinars
    * GitLab vision webinar
    * CE to EE webinar
    * SVN to Git webinar
    * 1 thought leadership or partner webinar a quarter
* conversationaldevelopment.com (prioritized for Q1 2017)
* remoteonly.org (prioritized for Q1 2017)

## Documentation Goals

* Create **well written** and **up-to-date** documentation.
* Enhance documentation articles with illustrations, animations and videos.
* Allow users to comment directly on documentation articles.
* Create a curriculum taking users from beginner to expert in Git and GitLab.
* Create a high quality responsive design.
* Allow users to easily browse and search the documentation.
* Generate MQLs

## Blog Goals

Detailed blog [handbook](/handbook/marketing/blog).

* Create **well written** and **engaging** content that shares GitLab's culture and user success stories.
* Monitor and promote existing popular content.
* Share the best posts in our newsletter with additional insights from the community discussions.
* Provide article formulas (recipes) for the team and community to use.
* Enhance blog posts with illustrations, animations and videos.
* Generate MQLs.

## Handbook Goals

* Improve the **writing quality** and organization of the GitLab handbook.
* The answer to any organizational questions should be "It's in the handbook".
* Share GitLab's culture.
* Generate MQLs.

todo: Webinar and Social Media goals.
