---
layout: markdown_page
title: Team Structure
page_class: org-structure
---

GitLab Inc. has at most five layers in the team structure:

1. CEO
1. Executives (e-team) consisting of CxO's and VP's
1. Directors
1. Leads
1. Individual contributors (IC's), which can be a [specialist](/jobs/specialist/) in one thing and be an [expert](/jobs/expert/) in multiple things.

The indentation below reflects the reporting relations. See a [graph of the organization chart](/team/structure/org-chart/).
You can see our complete team and who reports to whom on the [team page](https://about.gitlab.com/team/).

If there is one individual for a role this person is named here, if there are multiple there only is a link to a function description in order to reduce the duplication with the team page.
If there is a hyphen (-) in a line the part before hyphen is the name of the department and sometimes links to the relevant part of the [handbook](https://about.gitlab.com/handbook/).
The job titles link to the job descriptions.
Our open vacancies are at our [jobs page](https://about.gitlab.com/jobs/).

- [General](/handbook/) - [CEO](/jobs/chief-executive-officer/) - Sid Sijbrandij
  - [Marketing](/handbook/marketing/) - [VP of Marketing](/jobs/vp-of-marketing/) - vacancy
    - [Design](https://about.gitlab.com/handbook/marketing/design/) - [Designer](/jobs/designer/) - Luke Babb
    - [Demand generation](/handbook/marketing/demand-generation) - [Director of Demand Generation](/jobs/director-demand-generation) - Joe Lucas
      - [Senior Demand Generation Manager](/jobs/demand-generation-manager/) - (vacancy)
      - [Online Marketing](/handbook/marketing/demand-generation/online-marketing/) - [Online Marketing Manager](/jobs/online-marketing-manager/) - Mitchell Wright
      - [Marketing Operations Manager](https://about.gitlab.com/jobs/marketing-operations-manager/) - Jennifer Cordz
      - Inbound Business Development - [Business Development Team Lead - Inbound](/jobs/business-development-team-lead/) - Colton Taylor
      - [Field Marketing](/handbook/marketing/demand-generation/field-marketing/) - [Field Marketing Manager](/jobs/field-marketing-manager/) - Emily Kyle
      - [Business Development Representatives - Inbound](/jobs/business-development-representative/)
      - Outbound Business Development - [Business Development Team Lead - Outbound](/jobs/business-development-team-lead-outbound/)
    - [Product Marketing](/handbook/marketing/product-marketing/) - [Director of Product Marketing](/jobs/director-product-marketing/) - Amara Nwaigwe
      - [Partner Marketing](/handbook/marketing/product-marketing/partner-marketing/) - Partner/Channel Marketing Manager (vacancy)
      - [Content Marketing](/handbook/marketing/product-marketing/content-marketing/) - Content Marketing Manager (vacancy)
    - [Developer Relations](https://about.gitlab.com/handbook/marketing/developer-relations/)
      - [Developer Advocacy](/handbook/marketing/developer-relations/developer-advocacy/) - [Developer Advocate](/jobs/developer-advocate/)
      - [Technical Writing](https://about.gitlab.com/handbook/marketing/developer-relations/technical-writing/) - [Technical Writer](/jobs/technical-writer/)
      - [Community Advocacy](/handbook/marketing/developer-relations/community-advocacy/) - [Community Advocate](/jobs/community-advocate/) (vacancy)
  - [Sales](/handbook/sales/) - [CRO](/jobs/chief-revenue-officer/) - Chad Malchow
    - Americas Sales - [Account Executive](/jobs/account-executive/)
    - EMEA Sales - [Sales Director EMEA](/jobs/sales-director/) - Richard Pidgeon
      - [Account Executive](/jobs/account-executive/)
    - APAC Sales and channel - [Director of Global Alliances and APAC Sales](/jobs/director-of-global-alliances-and-apac-sales/) - Michael Alessio
    - Account Management - [Director of Global Account Management](https://about.gitlab.com/jobs/director-global-account-management/) (vacancy)
      - [Account Manager](/jobs/account-manager/)
    - Sales Operations - [Director of Sales Operations](https://about.gitlab.com/jobs/director-sales-operations/) - Francis Aquino
  - [People Operations](/handbook/people-operations/) - [Sr. Director of People Operations](/jobs/senior-director-of-people-operations/) Joan Parrow
    - [People Operations Generalist](/jobs/people-ops-generalist/)
    - [People Operations Coordinator](/jobs/people-ops-coordinator/)
  - [Finance](/handbook/finance) - [CFO](/jobs/chief-financial-officer/) - Paul Machle
    - Accounting - [Controller](/jobs/controller/) - Wilson Lau
  - [Technical Direction](/direction/) - [CTO](/jobs/chief-technology-officer/) - Dmitriy Zaporozhets
  - [Engineering](/handbook/engineering/) - [VP of Engineering](/jobs/vp-of-engineering/) - Stan Hu
    - Customer Success - [Director of Customer Success](/jobs/director-customer-success) (vacancy)
      - [Success Engineer](/jobs/success-engineer/)
      - [Success Engineer (APAC and Channel)](/jobs/Success-Engineer-APAC-and-Channel)(vacancy)
    - [Platform (Backend)](/handbook/backend/) - [Platform Lead](/jobs/backend-lead/) - Douwe Maan
      - [Developers](/jobs/developer/) who are platform specialists
    - [Discussion (Backend)](/handbook/backend#discussion) - [Discussion Lead](/jobs/backend-lead/) - Sean McGivern
      - [Developers](/jobs/developer/) who are discussion specialists
    - [CI (backend)](/handbook/backend#ci) - [CI lead](/jobs/ci-lead/) - Kamil Trzciński
      - [Developers](/jobs/developer/) who are CI specialists
    - Frontend - [Frontend Lead](/jobs/frontend-lead/) - Jacob Schatz
      - [Frontend Engineers](/jobs/frontend-engineer/)
    - UX - [UX Lead](/jobs/ux-lead/) - Allison Whilden
      - [UX Designers](/jobs/ux-designer/)
    - [Infrastructure](/handbook/infrastructure/) - [Infrastructure Lead](/jobs/infrastructure-lead/) - Pablo Carranza
      - [Production Engineers](/jobs/production-engineer/)
      - [Developers](/jobs/developer/) who are performance specialists
      - [Database specialists](/jobs/specialist/database/)
    - Build - [Build lead](/jobs/build-lead) - Marin Jankovski
      - [Developers](/jobs/developer/) who are packaging specialists
    - [Support](/handbook/support/) - [Support Lead (currently VP of Scaling)](/jobs/support-lead) - Ernst van Nierop
      - [Service Engineers](/jobs/service-engineer/)
    - [Edge](/handbook/edge/) - [Edge Lead](/jobs/edge-lead) - Rémy Coutable
       - [Developers](/jobs/developer/) who are issue triage specialists
       - [Developers](/jobs/developer/) who are maintainers of or specialist in projects without a lead
    - Security - Security Lead - Brian Neel
  - Scaling - [VP of Scaling](/jobs/vp-of-scaling/) - Ernst van Nierop
  - [General Product](/handbook/product/) - [VP of Product](/jobs/vice-president-of-product/) - Job van der Voort
  - [CI/CD Product](/handbook/product/#cicd) - [Head of Product](/jobs/head-of-product/) - Mark Pundsack
  - Strategic Partnerships - [Director of Strategic Partnerships](/jobs/director-strategic-partnerships/) - Eliran Mesika
